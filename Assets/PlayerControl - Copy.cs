﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerControl : MonoBehaviour
{

    public float CurMvSpd, BaseMvSpd, ModMvSpd;
    public float JmpPwr, JmpLimit;


    public Rigidbody thisRB;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        #region Controls
        if (Input.GetAxis("Horizontal") + Input.GetAxis("Vertical") != 0)
        {
            thisRB.AddForce((CurMvSpd * 100) *
                new Vector3(Input.GetAxis("Horizontal"),
                0,
                Input.GetAxis("Vertical"))
                );

        }
        else
        {
           // if (thisRB.velocity != Vector3.zero) { thisRB.velocity *= 0.9f; }
        }
        if (Input.GetKeyDown(KeyCode.Space)) { thisRB.AddForce(transform.up * (
             (JmpPwr * Input.GetAxis("Jump")) * 100)); }
        thisRB.velocity = new Vector3(
                Mathf.Clamp(thisRB.velocity.x, -10, 10),
                Mathf.Clamp(thisRB.velocity.y,-100,20),
                Mathf.Clamp(thisRB.velocity.z, -10, 10));
        //Rotation
        transform.Rotate(
            0,
            Input.GetAxis("Mouse X")*2f// + transform.localEulerAngles.y,
            ,0);
        
        #endregion
    }
}
