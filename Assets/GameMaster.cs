﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour {

    public Terrain thisTerrain;
    public GameObject playerObj;

	// Use this for initialization
	void Start () {
        GenTerrain();
        SpawnSite();
	}
	
	// Update is called once per frame
	void Update () {
		if(playerObj.transform.position.y<=-30)
        { SpawnSite(); }
	}

    void SpawnSite()
    {
        playerObj.transform.position= new Vector3(
            64 + Random.Range(0, 128),
            10 + Random.Range(-2, 10),
            64 + Random.Range(0, 128));
        playerObj.GetComponent<Rigidbody>().AddForce(0, -9.8f, 0, ForceMode.VelocityChange);
    }

    void GenTerrain()
    {
        int depth = 15;
        int width = 256;
        int height = 256;
        int scale = 20;

        thisTerrain.terrainData = GenerateTerrain(thisTerrain.terrainData, 
            width, height,scale,depth);
    }

    TerrainData GenerateTerrain(TerrainData x, int width, int height, int scale, int depth)
    {
        thisTerrain.terrainData.heightmapResolution = width + 1;
        thisTerrain.terrainData.size = new Vector3(width, depth, height);
        thisTerrain.terrainData.SetHeights(
            0, 0, GenTerrainHeight(width, height, scale));
        return thisTerrain.terrainData;
    }

    float [,] GenTerrainHeight(int width, int height, int scale)
    {
        float[,] heights = new float[width, height];
        for(int x=0;x<width; x++)
        {
            for(int y=0;y<width;y++)
            { heights[x, y] = CalculateHeight(x, y, width, height, scale); }
        }
        return heights;
    }

    float CalculateHeight(int x, int y, int width, int height, int scale)
    {
        float xCoord = (float)x / width * scale;
        float yCoord = (float)y / width * scale;

        return Mathf.PerlinNoise(xCoord, yCoord);
    } 
}
